<?php

namespace App\Service;

use App\Entity\Newsletter;
use App\Entity\User;
use App\Repository\NewsletterRepository;
use Doctrine\ORM\EntityManagerInterface;

class NewsletterService
{
    private $em;

    private $newsletterRepository;

    public function __construct(EntityManagerInterface $em, NewsletterRepository $newsletterRepository)
    {
        $this->em = $em;
        $this->newsletterRepository = $newsletterRepository;
    }

    public function addMail(Newsletter $newsletter, User $user): bool
    {
        $memberList = $this->newsletterRepository->findByUser($user);

        foreach ($memberList as $member) {
            if (($member->getEmail() === $newsletter->getEmail())) {
                return false;
            }
        }

        $newsletter->setUser($user);

        $this->em->persist($newsletter);
        $this->em->flush();

        return true;
    }
}
