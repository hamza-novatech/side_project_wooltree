<?php

namespace App\Form;

use App\Entity\Link;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LinkFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Link name',
            ])
            ->add('link', UrlType::class, [
                'label' => 'link adress'
            ])
            ->add('iconIsActivated', ChoiceType::class, [
                'choices'  => [
                    'Yes, activate the link icon' => true,
                    'No, do not activate the link icon' => false,
                ],
                'label' => 'Show link icon ?',
            ])
            ->add('publishFrom', DateTimeType::class, [
                'widget' => 'single_text',
                'label' => 'Publish from',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Link::class,
        ]);
    }
}
