<?php

namespace App\Controller;

use App\Entity\UserAvatar;
use App\Form\AvatarType;
use App\Form\CustomizationFormType;
use App\Repository\LinkRepository;
use App\Repository\NewsletterRepository;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/myprofile", name="user_profil")
     */
    public function index(LinkRepository $linkRepository)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();

        $links = $linkRepository->findByUserAndIsNotAchived($user);

        return $this->render('user/index.html.twig', [
            'links' => $links,
        ]);
    }

    /**
     * @Route("/myprofile/customization", name="user_customization")
     */
    public function customization(Request $request, UserService $userService, TranslatorInterface $translator)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();

        $form = $this->createForm(CustomizationFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
    
            $userService->edit($user);

            $this->addFlash('success', $translator->trans('Your modification is saved'));
            
            return $this->redirectToRoute('user_profil');
        }

        return $this->render('user/customization.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/myprofile/avatar", name="user_avatar")
     */
    public function avatar(Request $request, UserService $userService, TranslatorInterface $translator)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $userAvatar = new UserAvatar();

        $form = $this->createForm(AvatarType::class, $userAvatar);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userAvatar = $form->getData();
    
            $avatar = $userService->addAvatar($userAvatar);
            $userService->setAvatar($avatar, $this->getUser());

            $this->addFlash('success', $translator->trans('Your avatar is added'));
            
            return $this->redirectToRoute('user_profil');
        }

        return $this->render('user/avatar.html.twig', [
            'form' => $form->createView(),
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/myprofile/newsletter", name="user_newsletter")
     */
    public function newsletter(NewsletterRepository $newsletterRepository)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $newsletters = $newsletterRepository->findByUser($this->getUser());
        
        return $this->render('user/newsletter.html.twig', [
            'newsletters' => $newsletters,
        ]);
    }

    /**
     * @Route("/myprofile/newsletter/export", name="user_newsletter_export")
     */
    public function newsletterExport(NewsletterRepository $newsletterRepository)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $myVariableCSV = "Email;\n";
      
        $members = $newsletterRepository->findByUser($this->getUser());

        foreach ($members as $member) {
            $myVariableCSV .= $member->getEmail() . ";\n";
        }

        return new Response(
            $myVariableCSV,
            200,
            [
                 'Content-Type' => 'application/vnd.ms-excel',
                 "Content-disposition" => "attachment; filename=export_newsletter.csv"
              ]
        );
    }
}
