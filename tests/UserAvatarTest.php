<?php

namespace App\Tests;

use App\Entity\UserAvatar;
use PHPUnit\Framework\TestCase;

class UserAvatarTest extends TestCase
{
    public function testUserAvatarTrue()
    {
        $userAvatar  =new UserAvatar();

        $datetime =new \DateTime();

        $userAvatar->setImageName('image')
                   ->setImageSize('size')
                   ->setUpdatedAt($datetime);

        $this->assertTrue($userAvatar->getImageName() === 'image');
        $this->assertTrue($userAvatar->getImageSize() === 'size');
        $this->assertTrue($userAvatar->getUpdatedAt() === $datetime);
    }

    public function testUserAvatarFalse()
    {
        $userAvatar  =new UserAvatar();

        $datetime =new \DateTime();

        $userAvatar->setImageName('image')
                   ->setImageSize('size')
                   ->setUpdatedAt($datetime);

        $this->assertFalse($userAvatar->getImageName() === 'false');
        $this->assertFalse($userAvatar->getImageSize() === 'false');
        $this->assertFalse($userAvatar->getUpdatedAt() === new \DateTime());
    }

    public function testUserAvatarEmpty()
    {
        $userAvatar  =new UserAvatar();

        $this->assertEmpty($userAvatar->getImageName());
        $this->assertEmpty($userAvatar->getImageSize());
        $this->assertEmpty($userAvatar->getUpdatedAt());
    }
}
