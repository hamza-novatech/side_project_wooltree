<?php

namespace App\Controller;

use App\Repository\LinkRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function index(Request $request, UserRepository $userRepository, LinkRepository $linkRepository)
    {
        // Nous récupérons le nom d'hôte depuis l'URL
        $hostname = $request->getSchemeAndHttpHost();
        
        // On initialise un tableau pour lister les URLs
        $urls = [];

        // On ajoute les URLs "statiques"
        $urls[] = ['loc' => $this->generateUrl('home')];
        $urls[] = ['loc' => $this->generateUrl('app_register')];
        $urls[] = ['loc' => $this->generateUrl('app_login')];

        // On ajoute les URLs dynamiques des wooltree dans le tableau
        foreach ($userRepository->findAll() as $user) {
            $link = $linkRepository->findLastLinkByUser($user);

            if ($user->getImageName() === null) {
                $image = '/img/moockup.svg';
            } else {
                $image = '/uploads/avatar/'.$user->getImageName();
            }

            $images = [
                'loc' => $image, // URL to image
                'title' => $user->getPseudo()    // Optional, text describing the image
            ];

            $date = new \DateTime();

            $urls[] = [
                'loc' => $this->generateUrl('link', [
                    'slug' => $user->getSlug(),
                ]),
                'lastmod' => $link->getCreatedAt()->format('Y-m-d'),
                'image' => $images
            ];
        }
        // Fabrication de la réponse XML
        $response = new Response(
            $this->renderView('sitemap/index.html.twig', ['urls' => $urls,
                'hostname' => $hostname]),
            200
        );

        // Ajout des entêtes
        $response->headers->set('Content-Type', 'text/xml');

        // On envoie la réponse
        return $response;
    }

    /**
     * @Route("/robots.txt", name="robots", defaults={"_format"="txt"})
     */
    public function robotsTxt(Request $request)
    {
        // Nous récupérons le nom d'hôte depuis l'URL
        $hostname = $request->getSchemeAndHttpHost();

        // Fabrication de la réponse XML
        $response = new Response(
            $this->renderView('sitemap/robots.html.twig', [
                'url' => $hostname . '/sitemap.xml']),
            200
        );

        // Ajout des entêtes
        $response->headers->set('Content-Type', 'text/txt');

        // On envoie la réponse
        return $response;
    }
}
