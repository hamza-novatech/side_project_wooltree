<?php

namespace App\DataFixtures;

use App\Entity\Link;
use App\Entity\User;
use App\Entity\VisitLink;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        // User for test
        $user = new User();

        $plainPassword = 'password';
        $encodedPassword = $this->encoder->encodePassword($user, $plainPassword);

        $user->setEmail('test@test.com')
             ->setPassword($encodedPassword)
             ->setPseudo('test')
             ->setIsVerified(true)
             ->setColor($faker->randomElement(['_pastel', null]));

        $manager->persist($user);

        for ($i=0; $i < 10; $i++) {
            $user = new User();

            $plainPassword = $faker->password();
            $encodedPassword = $this->encoder->encodePassword($user, $plainPassword);
    
            $user->setEmail($faker->email())
                 ->setPassword($encodedPassword)
                 ->setPseudo($faker->name())
                 ->setIsVerified($faker->randomElement([true, false]))
                 ->setColor($faker->randomElement(['_pastel', null]));

            $manager->persist($user);

            for ($f=0; $f < 5; $f++) {
                $link = new Link();

                $link->setName($faker->words(3, true))
                     ->setLink($faker->url())
                     ->setCreatedAt(new \DateTime())
                     ->setUser($user)
                     ->setIsActivated($faker->randomElement([true, false]))
                     ->setIconIsActivated($faker->randomElement([true, false]))
                     ->setIsArchived($faker->randomElement([true, false]))
                     ->setScale($faker->randomElement([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]));

                $manager->persist($link);

                for ($g=0; $g < 50; $g++) {
                    $visitLink = new VisitLink();

                    $visitLink->setLink($link)
                              ->setVisitedOn($faker->dateTimeBetween('-1 years', 'now'));

                    $manager->persist($visitLink);
                }
            }
        }

        $manager->flush();
    }
}
