<?php

namespace App\Controller;

use App\Repository\LinkRepository;
use App\Repository\UserRepository;
use App\Repository\VisitLinkRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(VisitLinkRepository $visitLinkRepository, LinkRepository $linkRepository, UserRepository $userRepository)
    {
        $visit = $visitLinkRepository->findAll();

        $link = $linkRepository->findAll();

        $user = $userRepository->findAll();

        return $this->render('page/homepage.html.twig', [
            'controller_name' => 'PageController',
            'visit' => $visit,
            'link' => $link,
            'user' => $user,
        ]);
    }
}
