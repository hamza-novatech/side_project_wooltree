<?php

namespace App\Entity;

use App\Repository\LinkRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=LinkRepository::class)
 */
class Link
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Le nom ne peut être vide !")
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Le lien ne peut être vide !")
     */
    private $link;

    /**
     * @ORM\Column(type="datetime")
     */
    private $CreatedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $IsActivated;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="links")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=VisitLink::class, mappedBy="link")
     */
    private $visitLinks;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Slug(fields={"id", "CreatedAt", "name"})
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $iconIsActivated;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchived;

    /**
     * @ORM\Column(type="integer")
     */
    private $scale;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publishFrom;

    public function __construct()
    {
        $this->visitLinks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->CreatedAt;
    }

    public function setCreatedAt(\DateTimeInterface $CreatedAt): self
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }

    public function getIsActivated(): ?bool
    {
        return $this->IsActivated;
    }

    public function setIsActivated(bool $IsActivated): self
    {
        $this->IsActivated = $IsActivated;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|VisitLink[]
     */
    public function getVisitLinks(): Collection
    {
        return $this->visitLinks;
    }

    public function addVisitLink(VisitLink $visitLink): self
    {
        if (!$this->visitLinks->contains($visitLink)) {
            $this->visitLinks[] = $visitLink;
            $visitLink->setLink($this);
        }

        return $this;
    }

    public function removeVisitLink(VisitLink $visitLink): self
    {
        if ($this->visitLinks->contains($visitLink)) {
            $this->visitLinks->removeElement($visitLink);
            // set the owning side to null (unless already changed)
            if ($visitLink->getLink() === $this) {
                $visitLink->setLink(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getIconIsActivated(): ?bool
    {
        return $this->iconIsActivated;
    }

    public function setIconIsActivated(bool $iconIsActivated): self
    {
        $this->iconIsActivated = $iconIsActivated;

        return $this;
    }

    public function getIsArchived(): ?bool
    {
        return $this->isArchived;
    }

    public function setIsArchived(bool $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    public function getScale(): ?int
    {
        return $this->scale;
    }

    public function setScale(int $scale): self
    {
        $this->scale = $scale;

        return $this;
    }

    public function getPublishFrom(): ?\DateTimeInterface
    {
        return $this->publishFrom;
    }

    public function setPublishFrom(?\DateTimeInterface $publishFrom): self
    {
        $this->publishFrom = $publishFrom;

        return $this;
    }

    public function __toString()
    {
        return $this->link;
    }
}
