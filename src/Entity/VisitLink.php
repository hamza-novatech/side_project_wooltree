<?php

namespace App\Entity;

use App\Repository\VisitLinkRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VisitLinkRepository::class)
 */
class VisitLink
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $visitedOn;

    /**
     * @ORM\ManyToOne(targetEntity=Link::class, inversedBy="visitLinks")
     */
    private $link;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVisitedOn(): ?\DateTimeInterface
    {
        return $this->visitedOn;
    }

    public function setVisitedOn(\DateTimeInterface $visitedOn): self
    {
        $this->visitedOn = $visitedOn;

        return $this;
    }

    public function getLink(): ?Link
    {
        return $this->link;
    }

    public function setLink(?Link $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function __toString()
    {
        return $this->visitedOn->format('Y-m-d H:i:s');
    }
}
