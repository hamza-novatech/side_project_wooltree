<?php

namespace App\Controller\Admin;

use App\Entity\UserAvatar;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UserAvatarCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return UserAvatar::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
