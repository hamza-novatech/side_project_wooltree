<?php

namespace App\Events;

use App\Entity\Link;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class NewLinkEventSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->setIcon($args);
    }

    public function preupdate(LifecycleEventArgs $args)
    {
        $this->setIcon($args);
    }

    public function setIcon(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Link) {
            if (strpos($entity->getLink(), 'instagram') !== false) {
                $entity->setLogo('fab fa-instagram');
            } elseif (strpos($entity->getLink(), 'linkedin') !== false) {
                $entity->setLogo('fab fa-linkedin');
            } elseif (strpos($entity->getLink(), 'facebook') !== false) {
                $entity->setLogo('fab fa-facebook');
            } elseif (strpos($entity->getLink(), 'youtube') !== false) {
                $entity->setLogo('fab fa-youtube');
            } elseif (strpos($entity->getLink(), 'youtu.be') !== false) {
                $entity->setLogo('fab fa-youtube');
            } elseif (strpos($entity->getLink(), 'etsy') !== false) {
                $entity->setLogo('fab fa-etsy');
            } elseif (strpos($entity->getLink(), 'twitter') !== false) {
                $entity->setLogo('fab fa-twitter');
            } elseif (strpos($entity->getLink(), 'snapchat') !== false) {
                $entity->setLogo('fab fa-snapchat');
            } elseif (strpos($entity->getLink(), 'tiktok') !== false) {
                $entity->setLogo('fab fa-tiktok');
            } elseif (strpos($entity->getLink(), 'pinterest') !== false) {
                $entity->setLogo('fab fa-pinterest');
            } elseif (strpos($entity->getLink(), 'strava') !== false) {
                $entity->setLogo('fab fa-strava');
            } elseif (strpos($entity->getLink(), 'gitlab') !== false) {
                $entity->setLogo('fab fa-gitlab');
            } elseif (strpos($entity->getLink(), 'gitlab') !== false) {
                $entity->setLogo('fab fa-github');
            } else {
                $entity->setLogo('fas fa-link');
            }
        }
    }
}
